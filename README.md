# docker_mdns

Automatic docker mdns announcer designed to work with docker containers using labels.

## Installation

- `shards build`
- `cp bin/docker_mdns /usr/local/bin`
- `cp docker-mdns@.service /etc/systemd/system`
- `systemctl daemon-reload`
- `systemctl enable docker-mdns@{your_interface}`
- `systemctl start docker-mdns@{your_interface}`

## Usage

Label your local containers with:

```
mdns.host = chosen_hostname.local
```

## Development

Warning: This is a home solution / shit code.

## Contributing

1. Fork it (<https://gitlab.com/viraptor/docker_mdns/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Stanisław Pitucha](https://gitlab.com/viraptor) - creator and maintainer
